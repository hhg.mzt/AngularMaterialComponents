import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IGithubApi, IGithubIssue } from 'src/app/interfaces/table/employe.interface';
import { Observable } from 'rxjs';
import { SortDirection } from '@angular/material/sort';

@Injectable({
  providedIn: 'root'
})
export class GithubService {

  constructor(private _http: HttpClient) { }

  getRepoIssues(
    sort: string,
    order: SortDirection,
    page: number,
    perPage: number
  ):Observable<IGithubApi>{
    const href = 'https://api.github.com/search/issues';
    const requestUrl = `${href}?q=repo:angular/components&sort=${sort}&order=${order}&page=${
      page + 1
    }&per_page=${perPage}`;

    return this._http.get<IGithubApi>(requestUrl);

  }

  getRepoIssuesFilter(
    filter: string,
    sort: string,
    order: SortDirection,
    page: number,
    perpage: number
  ):Observable<IGithubApi>{
    const href = 'https://api.github.com/search/issues';
    const requestUrl = `${href}?q=${filter}+in:title+repo:angular/components&sort=${sort}&order=${order}&page=${page}&per_page=${perpage}`;

    return this._http.get<IGithubApi>(requestUrl);
  }

}
