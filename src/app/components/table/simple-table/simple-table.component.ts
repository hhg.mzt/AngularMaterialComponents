import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort, Sort } from '@angular/material/sort';

import { merge, Observable, of as observableOf, pipe } from 'rxjs';
import { catchError, map, startWith, switchMap, finalize } from 'rxjs/operators';
import { EmployeeService } from 'src/app/services/table/employee.service';

import {IEmployee, IEmployeeData } from 'src/app/interfaces/table/employe.interface';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-simple-table',
  templateUrl: './simple-table.component.html',
  styleUrls: ['./simple-table.component.css']
})
export class SimpleTableComponent {
  displayedColumns: string[] = [ 'id',
  'first_name',
  'last_name',
  'email',
  'avatar',];

  empTable?: IEmployeeData;
  totalData: number = 0;
  EmpData: IEmployee[] = [];
  dataSource!: MatTableDataSource<IEmployee>;
  isLoading: boolean = true;
  pageSizes: number[]= [3, 5, 7];
  @ViewChild('paginator') paginator!: MatPaginator;
  @ViewChild('empTbSort') empTbSort = new MatSort();
  searchKeywordFilter = new FormControl();

  constructor(private empService: EmployeeService){}

  ngOnInit() {
    //this.dataSource.data = this.EmpData;
    this.isLoading = true;
  }

  applyFilter(event: Event){
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getTableData$(pageNumber: number, pageSize: number){
    return this.empService.getEmployees(pageNumber, pageSize);
  }

  ngAfterViewInit(){
    //TODO: ¿Averiguar porque marca error y porque aún estando comentado funciona el paginator?
    //
    
    this.paginator.page
      .pipe(
        startWith({}),
        switchMap(() => {
          
          return this.getTableData$(
            this.paginator.pageIndex + 1,
            this.paginator.pageSize
          ).pipe(
            catchError(() => observableOf(null)),
              finalize(() => { 
                this.isLoading = false;
              })
            );
        }),
        map((empData) => {
          if (empData == null) return [];
          this.totalData = empData.total;
        
          return empData.data;
        })
      )
      .subscribe((empData) => {
        this.EmpData = empData;
        this.dataSource = new MatTableDataSource(this.EmpData);
      });
      
  }




}


