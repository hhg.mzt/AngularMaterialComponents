import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IEmployeeData } from 'src/app/interfaces/table/employe.interface';
import { Observable }from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private _http: HttpClient) { }

  public getEmployees(pageNumber: number, pageSize: number):Observable<IEmployeeData>{
    const url = `https://reqres.in/api/users?page=${pageNumber}&per_page=${pageSize}`;
    return this._http.get<IEmployeeData>(url);
  }

}
