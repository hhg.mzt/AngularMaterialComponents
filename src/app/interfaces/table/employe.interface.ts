export interface IEmployee{
    id: number;
    first_name: string;
    last_name: string;
    email: string;
    avatar: string;
}


export interface IEmployeeData{
    data: IEmployee[];
    page: number;
    per_page: number;
    total: number;
    total_pages: number;
}

export interface IGithubApi{
    items: IGithubIssue[];
    total_count: number;
}

export interface IGithubIssue {
    created_at: string;
    number: string;
    state: string;
    title: string;
}